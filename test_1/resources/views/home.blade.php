@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header text-center">Product</div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <h1>{{ $product }} </h1>
                                        <small> Records</small>
                                    </div>
                                </div>
                                <div class="card-footer text-center"><a href="/product">More Info</a></div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header text-center">Category</div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <h1>{{ $category }} </h1>
                                        <small> Records</small>
                                    </div>
                                </div>
                                <div class="card-footer text-center"><a href="/category">More Info</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
