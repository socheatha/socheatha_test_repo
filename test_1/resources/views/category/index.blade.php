@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Category Page</div>

                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div>
                    @endif
                    <a href="{{ route('category.create') }}" class="btn btn-primary">Add New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>N<sup>0</sup></th>
                                <th>Name</th>
                                <th>Date</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $record)
                            <tr>
                                <td>{{$record->id}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{$record->created_at}}</td>
                                <td class="text-right">
                                    <form method="post">
                                        @csrf
                                        <a href="{{ route('category.edit', $record->id)}}" class="btn btn-sm btn-warning">Edit</a>
                                        <button onclick="return confirm('are you sure to delete?')" formaction="{{ route('category.destroy', $record->id)}}" type="submit" value="DELETE" name="_method" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $records->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
