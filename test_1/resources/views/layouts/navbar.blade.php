<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/home">Dashboard</a>
</li>
<li class="nav-item dropdown">
    <a id="product" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Products <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="product">
        <a class="dropdown-item" href="{{ route('product.index') }}">  List </a>
        <a class="dropdown-item" href="{{ route('product.create') }}">  Create </a>
    </div>
</li>
<li class="nav-item dropdown">
    <a id="product" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
    Categories <span class="caret"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="product">
        <a class="dropdown-item" href="{{ route('category.index') }}">  List </a>
        <a class="dropdown-item" href="{{ route('category.create') }}">  Create </a>
    </div>
</li>