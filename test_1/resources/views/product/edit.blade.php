@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Product Page</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{ route('product.update',$product->id) }}" method="post" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name">Product Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="price">Price:</label>
                                    <input type="text" name="price" class="form-control" id="price" value="{{ $product->price }}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description:</label>
                                    <input type="text" name="description" class="form-control" id="description" value="{{ $product->description }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="profile">Profile:</label>
                                            <input type="file" name="profile" class="form-control"  onchange="readURL(this);" id="profile" value="{{ old('profile') }}">
                                            <input type="hidden" name="old_profile" class="form-control" id="profile" value="{{ $product->profile }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Category:</label>
                                            <select name="category_id" class="form-control" id="category">
                                                <option value="">please choose</option>
                                                @foreach($category as $data)
                                                    <option {{ $product->category_id==$data->id?'selected':'' }} value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <img width="100%" id="profile-img-tag" src="/images/product/{{ $product->profile }}" alt="{{ $product->profile }}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <br>
                                    <br>
                                    <input type="checkbox" name="status" id="status" {{ $product->status?'checked':'' }}>
                                    <label for="status">Status:</label>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('product.index') }}" class="btn btn-danger"> Back</a>
                        <button type="submit" class="btn btn-primary">Submit</button>                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
