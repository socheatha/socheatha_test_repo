@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Add Product Page</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{ route('product.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name">Product Name:</label>
                                    <input type="text" name="name" class="form-control {{ $errors->has('name')?'red_border':'' }}" id="name" value="{{ old('name') }}">
                                </div>
                                <div class="form-group">
                                    <label for="price">Price:</label>
                                    <input type="number" name="price" class="form-control {{ $errors->has('price')?'red_border':'' }}" id="price" value="{{ old('price') }}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description:</label>
                                    <input type="text" name="description" class="form-control {{ $errors->has('description')?'red_border':'' }}" id="description" value="{{ old('description') }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="profile">Profile:</label>
                                            <input type="file" name="profile" class="form-control" onchange="readURL(this);" id="profile" value="{{ old('profile') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Category:</label>
                                            <select name="category_id" class="form-control {{ $errors->has('category_id')?'red_border':'' }}" id="category">
                                                <option value="">please choose</option>
                                                @foreach($category as $data)
                                                    <option {{ old('category_id')==$data->id?'selected':'' }} value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <img width="100%" id="profile-img-tag" src="/images/product/thumbnail/blank.png" alt="" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <br>
                                    <br>
                                    <input type="checkbox" name="status" id="status" {{ old('status')=="on"?'checked':'' }}>
                                    <label for="status">Status</label>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('product.index') }}" class="btn btn-danger"> Back</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
