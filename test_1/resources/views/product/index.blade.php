@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Product Page</div>

                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div>
                    @endif
                    <a href="{{ route('product.create') }}" class="btn btn-primary">Add New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>N<sup>0</sup></th>
                                <th>Name</th>
                                <th class="text-center">Price</th>
                                <th>Description</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Profile</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $record)
                            <tr>
                                <td>{{$record->id}}</td>
                                <td>{{$record->name}}</td>
                                <th class="text-center">{{ number_format($record->price,2) }}</th>
                                <td>{{$record->description}}</td>
                                <td class="text-center">{{$record->status}}</td>
                                <td class="text-center"><img width="50" src="/images/product/thumbnail/{{ $record->profile }}" alt="{{ $record->profile }}" ></td>
                                <td>{{$record->cate_name}}</td>
                                <td>{{$record->created_at}}</td>
                                <td class="text-right">
                                    <form method="post">
                                        @csrf
                                        <a href="{{ route('product.edit', $record->id)}}" class="btn btn-sm btn-warning">Edit</a>
                                        <button onclick="return confirm('are you sure to delete?')" formaction="{{ route('product.destroy', $record->id)}}" type="submit" value="DELETE" name="_method" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $records->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
