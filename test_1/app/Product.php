<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',    
        'price',    
        'description',    
        'status',    
        'profile',    
        'category_id'  
    ];
}
