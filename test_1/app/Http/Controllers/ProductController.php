<?php

namespace App\Http\Controllers;

use Image;
use File;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records=Product::join('categories','categories.id','=','products.category_id')
            ->select(['products.*','categories.name as cate_name'])
            ->paginate(2);
        return view('product.index',compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category']=Category::all();
        return view('product.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->status = $request->has('status')?1:0;
        $request->validate([
            'name'=>'required|unique:products',
            'price'=>'required|numeric',
            'description'=>'required',
            'filename' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'category_id'=>'required|numeric'
        ]);
        $product = new Product($request->all());
        $product->status = $request->status;

        // image 
        if($request->hasFile('profile')) {
            $originalImage= $request->file('profile');
            $fileName = date('Ymdhis').'_'.Auth()->user()->id.'.'.$originalImage->getClientOriginalExtension();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/images/product/thumbnail/';
            $originalPath = public_path().'/images/product/';
            $thumbnailImage->save($originalPath.$fileName);
            $thumbnailImage->resize(50,50);
            $thumbnailImage->save($thumbnailPath.$fileName); 

            $product->profile = $fileName;
		}else{
            $product->profile = 'blank.png';
        }
        $product->save();
        return redirect('/product')->with('success', $request->name.' saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data['category']=Category::all();
        return view('product.edit', compact('product'))->with($data);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {   

        $request->status = $request->has('status')?1:0;
        $request->validate([
            'name'=>'required|'.Rule::unique('products')->ignore($product->id),
            'price'=>'required|numeric',
            'description'=>'required',
            'category_id'=>'required|numeric'
        ]);

        // image 
        if($request->hasFile('profile')) {
            $originalImage= $request->file('profile');
            $fileName = date('Ymdhis').'_'.Auth()->user()->id.'.'.$originalImage->getClientOriginalExtension();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/images/product/thumbnail/';
            $originalPath = public_path().'/images/product/';
            $thumbnailImage->save($originalPath.$fileName);
            $thumbnailImage->resize(50,50);
            $thumbnailImage->save($thumbnailPath.$fileName); 

            // after upload delete old image 
            $thumbnailPath = public_path().'/images/product/thumbnail/'.$product->profile;
            $originalPath = public_path().'/images/product/'.$product->profile;
            if(File::exists($thumbnailPath) && $product->profile!='blank.png'){
                File::delete($thumbnailPath);
            }
            if(File::exists($originalPath)){
                File::delete($originalPath);
            }

            $fileName = $fileName;
		}else{
            $fileName = $request->get('old_profile');
        }
        
        $product->update([
            'name' => $request->get('name'),
            'price' => $request->get('price'),
            'description' => $request->get('description'),
            'profile' => $fileName,
            'category_id' => $request->get('category_id'),
            'status' => $request->status
        ]);
        return redirect('/product')->with('success', $product->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {   
        $thumbnailPath = public_path().'/images/product/thumbnail/'.$product->profile;
        $originalPath = public_path().'/images/product/'.$product->profile;
        if(File::exists($thumbnailPath) && $product->profile!='blank.png'){
            File::delete($thumbnailPath);
        }
        if(File::exists($originalPath)){
            File::delete($originalPath);
        }
        


        $product->delete();
        return redirect('/product')->with('success', 'deleted!');
    }
}
